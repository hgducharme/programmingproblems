def nth_prime
	prime_numbers = [2]

	while prime_numbers[10000] == nil
		i = 3
		loop do 
			if i % 2 > 0 || i % 3 > 0 || i % 4 > 0 || i % 5 > 0 || i % 6 > 0 || i % 7 > 0 || i % 8 > 0 || i % 9 > 0 || i % 10 > 0
				prime_numbers << i
				i += 2
			end
			break if prime_numbers[10000] != nil
		end
	end

	puts "The 10,001st prime number is: #{prime_numbers[10000]}"
end

nth_prime